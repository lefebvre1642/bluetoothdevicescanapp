# BluetoothDeviceScanApp

## Application Summary
  * MVP (Model View Presenter) architecture
  * BroadcastReceivers - uses 2 BroadcastReceivers(1 to scan for devices and another to determine when the scan has finished). Both operating on a separate thread.
  * Few unit/instrumentation tests 
  * Activity containing ListView (could have used RecyclerView but figured few items were going to be in the list) with each row having a device name, address and rssi value. Along with a dynamic icon 
  * Min SDK: 19 
  * Target SDK: 28
  * No constraint layouts due to simple views

## Screenshot
<p align="center">
<img src="https://github.com/lecrane54/BluetoothDeviceScanApp/blob/master/screenshot.png" alt="Main Activity" width="250" height="400">
</p>

### Shortcuts
  * [ScannerActivity](https://github.com/lecrane54/BluetoothDeviceScanApp/blob/master/app/src/main/java/com/kylefe/bluetoothapp/view/ScannerActivity.kt)
  * [ScannerPresenter](https://github.com/lecrane54/BluetoothDeviceScanApp/blob/master/app/src/main/java/com/kylefe/bluetoothapp/presenter/ScannerPresenter.kt)
  * [Data Model](https://github.com/lecrane54/BluetoothDeviceScanApp/blob/master/app/src/main/java/com/kylefe/bluetoothapp/model/BluetoothDevice.kt)
  * [List adapter](https://github.com/lecrane54/BluetoothDeviceScanApp/blob/master/app/src/main/java/com/kylefe/bluetoothapp/adapter/BluetoothDeviceListAdapter.kt)
  * [Layout Directory](https://github.com/lecrane54/BluetoothDeviceScanApp/tree/master/app/src/main/res/layout)
  * [Unit tests](https://github.com/lecrane54/BluetoothDeviceScanApp/tree/master/app/src/test/java/com/kylefe/bluetoothapp)
  * [Instrumentation tests](https://github.com/lecrane54/BluetoothDeviceScanApp/tree/master/app/src/androidTest/java/com/kylefe/bluetoothapp)
