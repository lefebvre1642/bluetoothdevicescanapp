package com.kylefe.bluetoothapp.livedata

import android.arch.lifecycle.LiveData
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.HandlerThread

class FinishedDiscovering constructor(context: Context): LiveData<Boolean>() {
    private val c = context
    private val scanHandlerThread = HandlerThread("finish")

    override fun onInactive() {
        super.onInactive()
        stopDeviceDiscovery()
    }

    override fun onActive() {
        super.onActive()
        startDeviceDiscovery()
    }

    private fun startDeviceDiscovery(){
        scanHandlerThread.start()
        val looper = scanHandlerThread.looper
        val handler = Handler(looper)
        c.registerReceiver(scanReceiver, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED),null,handler)

    }

    private fun stopDeviceDiscovery(){
        c.unregisterReceiver(scanReceiver)
    }


    private val scanReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == intent.action) {
                postValue(true)
            }
        }
    }
}