package com.kylefe.bluetoothapp.livedata

import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.HandlerThread
import com.kylefe.bluetoothapp.model.BluetoothDevice

class DevicesLiveData constructor(context: Context): LiveData<BluetoothDevice>() {
    private val c = context
    private val scanHandlerThread = HandlerThread("scan")


    override fun onInactive() {
        super.onInactive()
        stopDeviceDiscovery()
    }

    override fun onActive() {
        super.onActive()
        startDeviceDiscovery()
    }

    private fun startDeviceDiscovery(){
        scanHandlerThread.start()
        val looper = scanHandlerThread.looper
        val handler = Handler(looper)
        c.registerReceiver(bluetoothReceiver, IntentFilter(android.bluetooth.BluetoothDevice.ACTION_FOUND),null,handler)
    }

    private fun stopDeviceDiscovery(){
        c.unregisterReceiver(bluetoothReceiver)
    }

    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (android.bluetooth.BluetoothDevice.ACTION_FOUND == intent.action) {
                val device = intent.getParcelableExtra<android.bluetooth.BluetoothDevice>(android.bluetooth.BluetoothDevice.EXTRA_DEVICE)
                if(!device.name.isNullOrEmpty() && !device.address.isNullOrEmpty()) { // Check if the device has a name and address
                    val rssi = intent.getShortExtra(android.bluetooth.BluetoothDevice.EXTRA_RSSI, java.lang.Short.MIN_VALUE)
                    val bDevice = BluetoothDevice(device.name, device.address, rssi)
                    postValue(bDevice)
                }
            }
        }
    }
}