package com.kylefe.bluetoothapp.view

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.content.pm.ActivityInfo
import android.content.res.ColorStateList
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_scanner.*
import android.os.*
import android.support.v7.widget.LinearLayoutManager
import com.kylefe.bluetoothapp.livedata.DevicesLiveData
import com.kylefe.bluetoothapp.livedata.FinishedDiscovering
import com.kylefe.bluetoothapp.adapter.DeviceListAdapter


/**
 * Main Activity responsible for checking permissions and starting/stopping BroadcastReceivers
 */
class ScannerActivity : AppCompatActivity(), View.OnClickListener, LifecycleOwner {
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private val permissionCode = 101
    private var mDevicesLiveData: DevicesLiveData? = null
    private var finishedLiveData: FinishedDiscovering? = null
    private var list = ArrayList<com.kylefe.bluetoothapp.model.BluetoothDevice>()
    private var adapter: DeviceListAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.kylefe.bluetoothapp.R.layout.activity_scanner)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        adapter = DeviceListAdapter(this,list)

        recycler.layoutManager = LinearLayoutManager(this)


        checkIfPermissionIsGranted()
        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        recycler.adapter = adapter

        fab.backgroundTintList = ColorStateList.valueOf(resources.getColor(com.kylefe.bluetoothapp.R.color.buttonColor))
        setupLiveData()
    }


    private fun setupLiveData(){
        mDevicesLiveData = DevicesLiveData(this)
        finishedLiveData = FinishedDiscovering(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            com.kylefe.bluetoothapp.R.id.fab -> {
                if (mBluetoothAdapter!!.isEnabled) {
                    list.clear()
                    if (mBluetoothAdapter!!.isDiscovering) {
                        cancelScan()
                    } else {
                         startScan()
                    }
                }
            }
        }
    }

    /**
     *  Stops the current scan and sets the FAB image accordingly
     */
    private fun cancelScan() {
        Toast.makeText(this, com.kylefe.bluetoothapp.R.string.cancelling, Toast.LENGTH_LONG).show()
        fab.setImageResource(com.kylefe.bluetoothapp.R.drawable.ic_bluetooth_normal)
        mBluetoothAdapter!!.cancelDiscovery()
        stopDiscoveringReceiver()
        stopFinishedReceiver()
    }

    /**
     * Starts the device scan and discovery finished receiver on a new thread
     * Also sets Image resources
     */
    private fun startScan() {
        Toast.makeText(this, com.kylefe.bluetoothapp.R.string.searching, Toast.LENGTH_LONG).show()
        fab.setImageResource(com.kylefe.bluetoothapp.R.drawable.ic_bluetooth_cancel)
        Thread(Runnable {
            mBluetoothAdapter!!.startDiscovery()
        }).start()
        startDiscoveringReceiver()
        startFinishedReceiver()
    }


    /**
     * Places an observer on the discovery receiver
     */
    private fun startFinishedReceiver(){
        finishedLiveData!!.observe(this, Observer {
            when(it){
                true -> {
                    runOnUiThread {
                        fab.setImageResource(com.kylefe.bluetoothapp.R.drawable.ic_bluetooth_normal)
                    }
                }
            }
        })
    }

    /**
     * Stop observing the receiver that waits for discovery to finish
     */
    private fun stopFinishedReceiver(){
        finishedLiveData!!.removeObserver {  }
    }


    /**
     * Start observing the device discovery receiver
     */
    private fun startDiscoveringReceiver(){
        mDevicesLiveData!!.observe(
            this,
            Observer< com.kylefe.bluetoothapp.model.BluetoothDevice> {
                list.add(it!!)
                list.sortBy { it.rssi }
                list.reverse()
                adapter!!.notifyDataSetChanged()
            })
    }

    /**
     * Remove the observer on the device discovery receiver
     */
    private fun stopDiscoveringReceiver(){
        mDevicesLiveData!!.removeObserver {  }
    }

    /**
     * Check version is at least lollipop and requests runtime permissions
     */
    private fun checkIfPermissionIsGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ), permissionCode
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            permissionCode -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                return
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!mBluetoothAdapter!!.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, 1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            1 -> when (resultCode) {
                Activity.RESULT_CANCELED -> return
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
