package com.kylefe.bluetoothapp.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.kylefe.bluetoothapp.R
import com.kylefe.bluetoothapp.model.BluetoothDevice

class DeviceListAdapter(private val context: Context, private val list: ArrayList<BluetoothDevice>) : RecyclerView.Adapter<DeviceListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.device_list, p0, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val bDevice = list[p1] // Get current Device

        /**
         * Set name, id and connection strength
         */
        p0.nameTextView.text = String.format(context.resources.getString(R.string.device_name),bDevice.name)
        p0.idTextView.text = String.format(context.resources.getString(R.string.device_address),bDevice.id)
        p0.rssiTextView.text = String.format(context.resources.getString(R.string.dbm),bDevice.rssi.toString())

        when(bDevice.rssi.toInt()){
            in -1 downTo -25 -> {
                p0.icon.setImageResource(R.drawable.ic_bluetooth_full)
            }
            in -26 downTo -50 -> {
                p0.icon.setImageResource(R.drawable.ic_bluetooth_3)
            }
            in -51 downTo -75 -> {
                p0.icon.setImageResource(R.drawable.ic_bluetooth_half)
            }
            in -76 downTo -100 -> {
                p0.icon.setImageResource(R.drawable.ic_bluetooth_no)
            }
            else ->{
                p0.icon.setImageResource(R.drawable.ic_bluetooth_no)
            }
        }

    }

    /**
     * ViewHolder pattern for efficiency
     * @param view
     */
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        var nameTextView = view.findViewById(R.id.device_name) as TextView
        var idTextView = view.findViewById(R.id.device_id) as TextView
        var rssiTextView = view.findViewById(R.id.device_rssi) as TextView
        var icon = view.findViewById(R.id.dynamic) as ImageView
    }
}