package com.kylefe.bluetoothapp.model

/**
 * Data object to hold Bluetooth Device Information
 */
data class BluetoothDevice(val name:String,val id:String,val rssi:Short)

