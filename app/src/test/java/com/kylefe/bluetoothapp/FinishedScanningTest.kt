package com.kylefe.bluetoothapp

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class FinishedScanningTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Test
    fun `live data test`() {
        val mutableLiveData = MutableLiveData<Boolean>()
        mutableLiveData.postValue(true)

        Assert.assertEquals(true, mutableLiveData.value)


    }
}