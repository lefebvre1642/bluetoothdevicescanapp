package com.kylefe.bluetoothapp

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.kylefe.bluetoothapp.model.BluetoothDevice
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class DeviceListTest {


    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Test
    fun `live data test`() {
        val mutableLiveData = MutableLiveData<BluetoothDevice>()
        val rssi : Short = -21
        mutableLiveData.postValue(BluetoothDevice("test","12345",-21))

        assertEquals("test", mutableLiveData.value!!.name)
        assertEquals("12345", mutableLiveData.value!!.id)
        assertEquals(rssi, mutableLiveData.value!!.rssi)

    }

}